module com.example.kaseer {
    requires javafx.controls;
    requires javafx.fxml;



    opens com.example.kaseer to javafx.fxml;
    opens com.example.kaseer.data to javafx.base;
    exports com.example.kaseer;
    opens com.example.kaseer.controller to javafx.fxml;

}