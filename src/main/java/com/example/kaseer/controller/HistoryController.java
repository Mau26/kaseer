package com.example.kaseer.controller;

import com.example.kaseer.data.OrderHistory;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.ArrayList;

import static java.lang.String.valueOf;

public class HistoryController {
    private OrderHistory orderHistory;
    public static ArrayList<OrderHistory> daftarRiwayatBelanja = new ArrayList<>();
    @FXML
    private Button detailButton;
    @FXML
    private Label jumlahItem;
    @FXML
    private Label tanggalBelanja;
    @FXML
    private Label totalHarga;
    @FXML
    private Label waktuBelanja;

    public void setData(OrderHistory orderHistory) {
//        this.orderHistory = orderHistory;
        tanggalBelanja.setText(orderHistory.getTanggal().toString());
        waktuBelanja.setText(orderHistory.getWaktu().toString());
        jumlahItem.setText(Integer.toString(orderHistory.getBarangBelanja().size()));
        System.out.println(orderHistory.getBarangBelanja().size());
        double totalHargas = 0;
        for(int i = 0; i < orderHistory.getBarangBelanja().size(); i++){
            totalHargas += orderHistory.getBarangBelanja().get(i).getTotalPrice();

        }
        totalHarga.setText(valueOf(totalHargas));
    }

}
