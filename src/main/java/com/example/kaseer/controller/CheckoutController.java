package com.example.kaseer.controller;

import com.example.kaseer.data.ItemCart;
import com.example.kaseer.data.OrderHistory;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;


import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.ResourceBundle;

import static com.example.kaseer.data.ItemRepository.*;
import static java.lang.String.valueOf;

public class CheckoutController implements Initializable {
    private OrderHistory riwayatBelanja;
    private ArrayList<ItemCart> itemCartList;
    @FXML
    private TableView<ItemCart> TableCheckout;

    @FXML
    private Button checkoutkan;

    @FXML
    private TableColumn<ItemCart, String> hargaTableColumn;

    @FXML
    private TableColumn<ItemCart, String> idTableColumn;

    @FXML
    private TableColumn<ItemCart, String> jumlahTableColumn;

    @FXML
    private Label kembalian;

    @FXML
    private TableColumn<ItemCart, String> namaBarangTableColumn;

    @FXML
    private Label totalBarang;

    @FXML
    private Label totalHargaSemua;

    @FXML
    private TableColumn<ItemCart, String> totalHargaTableColumn;

    @FXML
    private TextField uangBayar;

    @FXML
    void uangBayarInput(KeyEvent event) {
        if(Integer.parseInt(uangBayar.getText()) >= Integer.parseInt(totalHargaSemua.getText())) {
            kembalian.setText(valueOf((Integer.parseInt(uangBayar.getText()) - Integer.parseInt(totalHargaSemua.getText()))));
        }else{
            kembalian.setText("0");
        }

    }

    @FXML
    void checkoutBarang(MouseEvent event) {
        if(Integer.parseInt(uangBayar.getText()) < Integer.parseInt(totalHargaSemua.getText())) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Uang anda kurang!");
            alert.showAndWait();
        }else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Terima Kasih! Selamat Belanja Kembali");
            alert.showAndWait();
            riwayatBelanja = new OrderHistory(itemList, LocalDate.now(), LocalTime.now());
            updateDaftarRiwayat(riwayatBelanja);
//            clearBarangBelanjaaan();

        }

    }
    private void updateDaftarRiwayat(OrderHistory orderHistory){
        orderHistories.add(riwayatBelanja);
    }
    private void clearBarangBelanjaaan(){
        cartList.clear();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        kembalian.setText("0");
        ArrayList<ItemCart> itemCarts = cartList;

        jumlahTableColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
        namaBarangTableColumn.setCellValueFactory(new PropertyValueFactory<>("itemName"));
        idTableColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        hargaTableColumn.setCellValueFactory(new PropertyValueFactory<>("itemPrice"));
        totalHargaTableColumn.setCellValueFactory(new PropertyValueFactory<>("totalPrice"));
        TableCheckout.getItems().setAll(itemCarts);

        int jumlahBeli = 0;
        int totalHarga = 0;
        for(int i = 0; i < itemCarts.size(); i++){
            jumlahBeli += itemCarts.get(i).getAmount();
            totalHarga += itemCarts.get(i).getTotalPrice();
        }
        totalBarang.setText(valueOf(jumlahBeli));
        totalHargaSemua.setText(valueOf(totalHarga));



    }
}
