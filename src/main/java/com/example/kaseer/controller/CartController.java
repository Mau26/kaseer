package com.example.kaseer.controller;

import com.example.kaseer.data.FinalValue;
import com.example.kaseer.data.ItemCart;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import static com.example.kaseer.data.ItemRepository.itemList;
import static java.lang.String.valueOf;

import static com.example.kaseer.data.ItemRepository.cartList;

public class CartController {
    @FXML
    private Label HargaBarangBelanjaan;
    @FXML
    private Label IDBarangBelanja;
    @FXML
    private Label JumlahBarangBeliBelanjaan;
    @FXML
    private Label NamaBelanjaan;
    @FXML
    private Label TotalHargaBelanjaan;
    @FXML
    void deleteItemBelanja(MouseEvent event) {
        for(int i = 0; i < itemList.size();i++){
            if(itemList.get(i).getId() == Integer.parseInt(IDBarangBelanja.getText())){
                itemList.remove(i);
            }
        }
    }

    public void setData(ItemCart itemCart){
        IDBarangBelanja.setText(valueOf(itemCart.getId()));
        NamaBelanjaan.setText(itemCart.getItemName());
        JumlahBarangBeliBelanjaan.setText("Jumlah: " + itemCart.getAmount());
        HargaBarangBelanjaan.setText(FinalValue.CURRENCY + itemCart.getItemPrice());
        TotalHargaBelanjaan.setText(valueOf(itemCart.getTotalPrice()));
    }
}
