package com.example.kaseer.controller;

import com.example.kaseer.Main;
import com.example.kaseer.data.ItemStock;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.kaseer.data.ItemRepository.stocks;
import static java.lang.String.valueOf;

public class StockController {
    private boolean isEdited;
    private ModifItemController modifItemController;
    private ArrayList<ItemStock> barangStocksS = new ArrayList<>();
    @FXML
    private Label HargaBarangItemStock;
    @FXML
    private Label IDItemStock;
    @FXML
    private Label JumlahItemStock;
    @FXML
    private Label NamaBarangItemStock;
    @FXML
    void DeleteStockItem(MouseEvent event) {
        for(int i = 0; i < stocks.size();i++){
            if(stocks.get(i).getId() == Integer.parseInt(IDItemStock.getText())){
                stocks.remove(i);
            }
        }
    }

    @FXML
    void EditStockItem(MouseEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("/com/example/kaseer/ModifItemStock.fxml"));
            Parent root = fxmlLoader.load();

            ModifItemController controller = fxmlLoader.getController();
            controller.setIDEditItemStock(IDItemStock);

            Scene scene = new Scene(root, 300, 310);

            Stage stage = new Stage();
            stage.setTitle("Edit Item");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @FXML
    void initialize(){}
    public void setData(ItemStock item) {
        IDItemStock.setText(valueOf(item.getId()));
        NamaBarangItemStock.setText(item.getItemName());
        HargaBarangItemStock.setText(valueOf(item.getItemPrice()));
        JumlahItemStock.setText(valueOf(item.getAmount()));
    }
}
