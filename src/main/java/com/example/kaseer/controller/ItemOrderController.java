package com.example.kaseer.controller;

import com.example.kaseer.data.ItemCart;
import com.example.kaseer.data.ItemStock;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import static com.example.kaseer.data.ItemRepository.cartList;
import static com.example.kaseer.data.ItemRepository.itemList;
import static java.lang.String.valueOf;

public class ItemOrderController {

    private ItemStock itemStock;
    @FXML
    private TextField AmountPesan;
    @FXML
    private Label ItemNamePesan;
    @FXML
    private Label ItemPricePesan;
    @FXML
    private Label StockItemPesan;
    @FXML
    private Label TotalHargaPesan;
    @FXML
    private Label itemPesanID;
    @FXML
    void inputJumlah(KeyEvent event) {
        try {
            int jumlahPesan = Integer.parseInt(valueOf(AmountPesan.getText()));
            int hargaBarang = itemStock.getItemPrice();
            TotalHargaPesan.setText(valueOf(jumlahPesan * hargaBarang));
        }catch(Exception e){
            int jumlahPesan = 1;
            int hargaBarang = itemStock.getItemPrice();
            TotalHargaPesan.setText(valueOf(jumlahPesan * hargaBarang));
        }

    }
    @FXML
    void addCart(MouseEvent event) {
        tambahBelanja();
    }


    private void tambahBelanja(){
        itemList.add(new ItemCart(Integer.parseInt(itemPesanID.getText()), ItemNamePesan.getText(), itemStock.getItemPrice(),
                Integer.parseInt(AmountPesan.getText()), Integer.parseInt(TotalHargaPesan.getText())));
    }

    public void setData(ItemStock itemStock) {
        this.itemStock = itemStock;
        ItemNamePesan.setText(itemStock.getItemName());
        ItemPricePesan.setText(valueOf(itemStock.getItemPrice()));
        StockItemPesan.setText(valueOf(itemStock.getAmount()));
        itemPesanID.setText(valueOf(itemStock.getId()));
    }

}
