package com.example.kaseer.controller;


import com.example.kaseer.Main;
import com.example.kaseer.data.Item;
import com.example.kaseer.data.ItemStock;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.kaseer.data.ItemRepository.stocks;

public class ModifItemController {
    private boolean isEdited = false;

    @FXML
    private TextField HargaTambahItem;

    @FXML
    private TextField IDTambahItem;

    @FXML
    private TextField NamaTambahItem;

    @FXML
    private TextField StockTambahItem;

    @FXML
    private Button SubmitModifItem;

    @FXML
    private Label judulAddItem;
    @FXML
    private TextField HargaModifItem;
    @FXML
    private TextField NamaModifItem;
    @FXML
    private TextField StockModifItem;
    @FXML
    private Label IDEditItemStock;

    @FXML
    void Daftarkan(MouseEvent event) {
        daftarkanItem();
    }
    @FXML
    void editItem(MouseEvent event) {
        editItemStock();
    }
    private void daftarkanItem(){
        if(checkDuplicate(Integer.parseInt(IDTambahItem.getText()))) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Id nomor " + IDTambahItem.getText() + " telah terdaftar! Silakan mendaftarkan menggunakan id berbeda");
            alert.showAndWait();
        } else{

            stocks.add(new ItemStock(Integer.parseInt(IDTambahItem.getText()), NamaTambahItem.getText(), Integer.parseInt(HargaTambahItem.getText()),
                    Integer.parseInt(StockTambahItem.getText())));
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Item Berhasil ditambahkan! Silakan tutup jendela tambah item jika tidak ada lagi yang ingin ditambahkan");
            alert.showAndWait();
        }
    }
    private boolean checkDuplicate(int id){
        for(Item barang: stocks){
            if(barang.getId() == id){
                return true;
            }
        }
        return false;
    }
    private void editItemStock(){
        for(int i = 0; i < stocks.size();i++){
            if(stocks.get(i).getId() == Integer.parseInt(IDEditItemStock.getText())){
                stocks.get(i).setItemName(NamaModifItem.getText());
                stocks.get(i).setItemPrice(Integer.parseInt(HargaModifItem.getText()));
                stocks.get(i).setAmount(Integer.parseInt(StockModifItem.getText()));
                break;
            }
        }
        isEdited = true;
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Item Berhasil diedit! Silakan tutup jendela tambah item jika tidak ada lagi yang ingin ditambahkan");
        alert.showAndWait();
    }

    public void setIDEditItemStock(Label IDEditItemStock) {
        this.IDEditItemStock = IDEditItemStock;
    }
}
