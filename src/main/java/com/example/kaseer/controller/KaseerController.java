package com.example.kaseer.controller;


import com.example.kaseer.Main;
import com.example.kaseer.data.OrderHistory;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.kaseer.data.ItemRepository.*;


public class KaseerController {
    @FXML
    private GridPane GridPane;
    @FXML
    private Label JudulMenu;
    @FXML
    void ClickDaftarBarang(MouseEvent event) {
        String resource = "/com/example/kaseer/ItemStock.fxml";
        stockMenu(resource);
    }
    @FXML
    void ClickDaftarBelanja(MouseEvent event) {
        String resource = "/com/example/kaseer/ItemCart.fxml";
        cartMenu(resource);
    }
    @FXML
    void ClickPesan(MouseEvent event) {
        String resource = "/com/example/kaseer/ItemOrder.fxml";
        orderMenu(resource);

    }
    @FXML
    void ClickRiwayatBelanja(MouseEvent event) {
        String resource = "/com/example/kaseer/History.fxml";
        historyMenu(resource);


    }
    @FXML
    void InputItemBaru(MouseEvent event) {
        String resource = "/com/example/kaseer/addItem.fxml";
        itemBaru(resource);

    }
    @FXML
    void checkOutBelanja(MouseEvent event) {
        String resource = "/com/example/kaseer/checkout.fxml";
        checkout(resource);
    }
    @FXML
    void initialize(){}

    private void itemBaru(String resource){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource(resource));
            Scene scene = new Scene(fxmlLoader.load(), 300, 310);
            Stage stage = new Stage();
            stage.setTitle("Tambah Item");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private void orderMenu(String resource){
        int column = 0;
        int row = 0;
        GridPane.getChildren().clear();
        try {
            for (int i = 0; i < stocks.size(); i++) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource(resource));
                AnchorPane anchorPane = fxmlLoader.load();

                ItemOrderController item = fxmlLoader.getController();
                item.setData(stocks.get(i));


                GridPane.add(anchorPane, column, row++);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    private void stockMenu(String resource){
        int column = 0;
        int row = 0;
        GridPane.getChildren().clear();
        try {
            for (int i = 0; i < stocks.size(); i++) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource(resource));
                AnchorPane anchorPane = fxmlLoader.load();

                StockController item = fxmlLoader.getController();
                item.setData(stocks.get(i));


                GridPane.add(anchorPane, column, row++);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
    private void cartMenu(String resource){
        int column = 0;
        int row = 0;
        GridPane.getChildren().clear();
        try {
            for (int i = 0; i < stocks.size(); i++) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource(resource));
                AnchorPane anchorPane = fxmlLoader.load();

                CartController item = fxmlLoader.getController();
                item.setData(cartList.get(i));


                GridPane.add(anchorPane, column, row++);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    private void checkout(String resource){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource(resource));
            Scene scene = new Scene(fxmlLoader.load(), 600, 660);
            Stage stage = new Stage();
            stage.setTitle("Kaseer");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void historyMenu(String resource){
        int column = 0;
        int row = 0;
        GridPane.getChildren().clear();
        try {
            for (int i = 0; i < orderHistories.size(); i++) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource(resource));
                AnchorPane anchorPane = fxmlLoader.load();

                HistoryController controller = fxmlLoader.getController();
                controller.setData(orderHistories.get(i));

                GridPane.add(anchorPane, column, row++);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }


}
