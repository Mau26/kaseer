package com.example.kaseer.data;

public class ItemCart extends Item{

    private int amount;
    private int totalPrice;

    public ItemCart(int id, String itemName, int itemPrice, int amount, int totalPrice) {
        super(id, itemName, itemPrice);
        this.amount = amount;
        this.totalPrice = totalPrice;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
}
