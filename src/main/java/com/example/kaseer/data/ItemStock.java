package com.example.kaseer.data;

public class ItemStock extends Item{
    private int amount;

    public ItemStock(int id, String itemName, int itemPrice, int amount) {
        super(id, itemName, itemPrice);
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
