package com.example.kaseer.data;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class OrderHistory {
    private ArrayList<ItemCart> barangBelanja;
    private LocalDate tanggal = LocalDate.now();
    private LocalTime waktu = LocalTime.now();

    public OrderHistory(ArrayList<ItemCart> barangBelanja, LocalDate tanggal, LocalTime waktu) {
        this.barangBelanja = barangBelanja;
        this.tanggal = tanggal;
        this.waktu = waktu;
    }

    public ArrayList<ItemCart> getBarangBelanja() {
        return barangBelanja;
    }

    public void setBarangBelanja(ArrayList<ItemCart> barangBelanja) {
        this.barangBelanja = barangBelanja;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public LocalTime getWaktu() {
        return waktu;
    }

    public void setWaktu(LocalTime waktu) {
        this.waktu = waktu;
    }
}
